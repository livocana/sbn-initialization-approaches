import numpy as np
from torch import tensor
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import matplotlib.pyplot as plt
import matplotlib
import torch
from torch.utils.tensorboard import SummaryWriter
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from image_processing import *
from architecture import *
from binary_conv import *


def read_data(train_batch_size, test_batch_size):
    dataset_loader = DatasetLoader(train_batch_size=train_batch_size, test_batch_size=test_batch_size)
    return dataset_loader.load_cifar10()

def train(dataloader, base_lr=0.001, momentum=0.9, epochs=1, name="allcnn_model"):
    writer = SummaryWriter()
    model = ConvolutionalNet()
    # device = torch.device("cuda:6")
    # model = model.to(device)
    loss = nn.CrossEntropyLoss()
    train_loader = dataloader.get_trainloader()
    test_loader = dataloader.get_testloader()
    n_train = dataloader.get_ntrain()
    n_test = dataloader.get_ntest()

    train_losses = []
    validation_losses = []
    validation_accuracies = []
    batch_losses = []

    name = name + "_" + str(epochs) + ".pt"
    gamma = 0.2
    gamma_lr = np.exp(np.log(0.01) / epochs)
    print("total epochs: ", epochs)
    for epoch in range(epochs):
        model.train()
        lr = gamma_lr ** epoch * base_lr
        optimizer = optim.SGD(model.parameters(), lr=lr, momentum=momentum)

        print(f"\nEPOCH: {epoch}")
        # will accumulate total loss over the dataset
        batch_size = int(n_train / 32 * 0.05)
        L, ewa_train_accuracy = 0, np.nan
        L_batch, ewa_losses = 0, np.nan
        batch_correct, ewa_accuracies = 0, np.nan
        total_correct = 0

        # loop fetching a mini-batch of data at each iteration
        for i, (data, target) in enumerate(train_loader):
            # data, target = data.to(device), target.to(device)
            y = model.forward(data)

            pred = y.data.max(1, keepdim=True)[1]
            batch_correct += pred.eq(target.data.view_as(pred)).sum().item()
            total_correct += pred.eq(target.data.view_as(pred)).sum().item()

            l = loss(y, target)
            L += l.sum().item()
            L_batch += l.sum().item()

            optimizer.zero_grad()
            l.mean().backward()
            optimizer.step()
            if (i + 1) % batch_size == 0:  # every 10% of the data
                index = 10 * epoch + (i // batch_size + 1)
                L_batch = L_batch / batch_size / 32
                batch_losses.append(L_batch)
                if ewa_losses is np.nan:
                    ewa_losses = L_batch
                else:
                    ewa_losses = (1 - gamma) * ewa_losses + gamma * (L_batch)

                if ewa_accuracies is np.nan:
                    ewa_accuracies = batch_correct / batch_size / 32
                else:
                    ewa_accuracies = (1 - gamma) * ewa_accuracies + gamma * batch_correct / batch_size / 32

                print(
                    f'\t Batch: {index}  batch loss {L_batch}\t ewa losses {ewa_losses} \t ewa_accuracies {ewa_accuracies} ')

                L_batch = 0
                batch_correct = 0


        L_test = 0
        correct = 0

        model.eval()
        with torch.no_grad():
            for i, (data, target) in enumerate(test_loader):
                # data, target = data.to(device), target.to(device)
                y = model(data)
                pred = y.data.max(1, keepdim=True)[1]
                correct += pred.eq(target.data.view_as(pred)).sum()

                l = loss(y, target)

                # accumulate the total loss as a regular float number (important to sop graph tracking)
                L_test += l.sum().item()

        # calculating metrics, writing to dashboard and printing
        test_accuracy = correct.item() / n_test
        test_loss = L_test / n_test
        train_loss = L / n_train
        print(total_correct, n_train)
        train_accuracy = total_correct / n_train

        train_losses.append(train_loss)
        validation_losses.append(test_loss)
        validation_accuracies.append(test_accuracy)
        writer.add_scalar("Epoch loss/validation", test_loss, epoch)
        writer.add_scalar("Epoch accuracy/validation", test_accuracy, epoch)
        writer.add_scalar("Epoch accuracy/train", train_accuracy, epoch)



        print(f'Epoch {epoch} summary:  mean train loss: {train_loss}\t mean validation loss: {test_loss} '
              f'\t validation accuracy: {correct} / {n_test} [{test_accuracy}%] \n', )
        writer.add_scalar("Epoch loss/train",train_loss, epoch)
    torch.save(model.get_layers().state_dict(), "models/" + name)
    writer.flush()
    writer.close()


def test_accuracy(model, test_loader, n_test, flag=False):
    correct = 0
    model.eval()
    with torch.no_grad():
        for i, (data, target) in enumerate(test_loader):
            y = model(data)
            pred = y.data.max(1, keepdim=True)[1]
            if flag:
                print(pred.eq(target.data.view_as(pred)).sum(),  '/', y.shape[0])
            correct += pred.eq(target.data.view_as(pred)).sum()
    return correct.item() / n_test


def test_accuracy_multisample(model: nn.Sequential, test_loader: torch.utils.data.DataLoader, n_test: int, fw_method):
    correct = 0
    model.eval()
    with torch.no_grad():
        for i, (data, target) in enumerate(test_loader):
            y = fw_method.forward(model, x=data)
            y = fw_method.mean(y)
            pred = y.data.max(1, keepdim=True)[1]
            print(f"batch correct: {pred.eq(target.data.view_as(pred)).sum()} \ {pred.size()[0]}")
            correct += pred.eq(target.data.view_as(pred)).sum()
    return correct.item() / n_test


def test_accuracy_multisample_check(model: nn.Sequential, test_loader: torch.utils.data.DataLoader, n_test: int, fw_method, copies):
    correct = 0
    model.eval()
    with torch.no_grad():
        for i, (data, target) in enumerate(test_loader):
            results = []
            for j in range(copies):
                results.append(fw_method.forward(model, x_repr=data))
            y = torch.mean(torch.stack(results), dim=0)
            print(y.shape, torch.cat(results).shape)
            pred = y.data.max(1, keepdim=True)[1]
            print(f"batch correct: {pred.eq(target.data.view_as(pred)).sum()} \ {pred.size()[0]}")
            correct += pred.eq(target.data.view_as(pred)).sum()
    return correct.item() / n_test


def print_statistics_batch(real_net, binary_net, scales, linear_layer_index, data, _):
    fig = make_subplots(rows=2, cols=(len(real_net) - 2) // 2, start_cell="top-left")
    legend = True
    x1, x0 = data, data
    sign_layer_index = -1

    for index in range(len(real_net)):
        layer = real_net[index]
        binlayer = binary_net[index]

        if index == 0 or index == linear_layer_index:
            x1 = binlayer(x1)
            x0 = layer(x0)
            print("linear, i = ", index)
            print("means: ", torch.mean(x0), torch.mean(x1))
            print("stds: ", torch.std(x0), torch.std(x1))

        elif isinstance(layer, torch.nn.modules.conv.Conv2d):
            x0 = layer(x0)
            x1 = SBNFunctionality.binary_conv_forward(binlayer, x1)
            # x1 = binlayer(x1)
            print("conv, i = ", index)
            print("means: ", torch.mean(x0), torch.mean(x1))
            print("stds: ", torch.std(x0), torch.std(x1))

        elif isinstance(layer, torch.nn.modules.activation.LeakyReLU):
            x0 = layer(x0)
            sign_layer_index += 1
            # print(torch.mean(sign_scale[sign_layer_index]))
            x1 = SBNFunctionality.sign_activation(x1, scales[sign_layer_index])
            # x1 = binlayer(x1)
            print("act, i = ", index)
            print("means: ", torch.mean(x0), torch.mean(x1))
            print("stds: ", torch.std(x0), torch.std(x1))

        else:
            x0 = layer(x0)
            x1 = binlayer(x1)

        if len(x0.shape) > 3:
            fig.append_trace(go.Histogram(x=x0[:, 7, :, :].flatten().detach().numpy(),
                                          marker_color='red', showlegend=legend, name='real-valued', legendgroup='real-valued'), index%2 + 1, index//2 + 1)
            fig.append_trace(go.Histogram(x=x1[:, 7, :, :].flatten().detach().numpy(),
                                          marker_color='blue', name='binary', legendgroup='binary', showlegend=legend), index%2 + 1, index//2 + 1)
            legend = False
            fig.update_xaxes(title_text=layer.__class__.__name__,  row=index%2 + 1, col=index//2 + 1)

    fig.update_layout(height=600, width=1500)
    fig.show()



# def test_model(model, test_loader, n_test):
#     correct = 0
#     model.eval()
#     with torch.no_grad():
#         for i, (data, target) in enumerate(test_loader):
#             for layer in model:
#                 if isinstance(layer, TrinaryConv):
#




# if __name__ == "__main__":
#     # train(base_lr=0.001, epochs=1)
#     dataloader = read_data()
#     test_loader = dataloader.get_testloader()
#     ntest = dataloader.get_ntest()














