import numpy as np
import torch
from architecture import *
import pickle
import sys
from train_cnn import *

def save_result(result, filename):
    with open(filename, 'wb') as file:
        pickle.dump(result, file)

sigma = float(sys.argv[1])
scale_type = int(sys.argv[2])
ncopies = eval(sys.argv[3])
layer_index = eval(sys.argv[4])
filename = f'relu_exp_results/{sys.argv[5]}.pickle'

dataloader = DatasetLoader(train_batch_size=256, test_batch_size=100).load_cifar10()
test_loader = dataloader.get_testloader()
ntest = dataloader.get_ntest()
train_loader = dataloader.get_trainloader()
ntrain = dataloader.get_ntrain()

model = ConvolutionalNet().get_layers()
model.load_state_dict(torch.load("models/allcnn_model_120.pt", map_location=torch.device('cpu')))

converter = Converter(dataloader.get_trainloader(), dataloader.get_ntrain(), dataloader.get_testloader(), dataloader.get_ntest(), sigma=sigma, scale_type=scale_type)
model = converter.eliminate_batchnorm(model)
result = {'sigma': sigma, 'scale_type': scale_type, 'data':{}}


for i, n in enumerate(ncopies):
    copy_result = {}
    for j, layer_i in enumerate(layer_index):
        modeln = converter.scale_single_layer(model, layer_i, number_copy=n, plot_weights_new=False, plot=False, flag=False)
        fw_method = ForwardSample(8)
        acc_test = test_accuracy_multisample (modeln, test_loader, ntest, fw_method=fw_method)
        copy_result.update ({layer_i: {'test': acc_test}})

    print(f"copies: {i}/{len(ncopies)}")
    result['data'].update({n: copy_result})

print(result)
save_result(result, filename)







