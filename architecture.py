from binary_conv import *
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import warnings


class ConvolutionalNet(nn.Module):

    def __init__(self):
        super(ConvolutionalNet, self).__init__()
        self.layers = nn.Sequential(nn.Conv2d(3, 96, kernel_size=3, stride=1),
                                    nn.BatchNorm2d(96),
                                    nn.LeakyReLU(),
                                    nn.Conv2d(96, 96, kernel_size=3, stride=1),
                                    nn.BatchNorm2d(96),
                                    nn.LeakyReLU(),
                                    nn.Conv2d(96, 96, kernel_size=3, stride=2),
                                    nn.BatchNorm2d(96),
                                    nn.LeakyReLU(),
                                    nn.Conv2d(96, 192, kernel_size=3, stride=1),
                                    nn.BatchNorm2d(192),
                                    nn.LeakyReLU(),
                                    nn.Conv2d(192, 192, kernel_size=3, stride=1),
                                    nn.BatchNorm2d(192),
                                    nn.LeakyReLU(),
                                    nn.Conv2d(192, 192, kernel_size=3, stride=2),
                                    nn.BatchNorm2d(192),
                                    nn.LeakyReLU(),
                                    nn.Conv2d(192, 192, kernel_size=3, stride=1),
                                    nn.BatchNorm2d(192),
                                    nn.LeakyReLU(),
                                    nn.Conv2d(192, 192, kernel_size=1, stride=1),
                                    nn.BatchNorm2d(192),
                                    nn.LeakyReLU(),
                                    nn.Conv2d(192, 10, kernel_size=1, stride=1),
                                    nn.BatchNorm2d(10),
                                    nn.AdaptiveAvgPool2d(1),
                                    nn.Flatten(),
                                    nn.LogSoftmax(1)
                                    )

    def get_layers(self):
        return self.layers

    def forward(self, x):
        for l in self.layers:
            x = self.forward(x)
        return x


class Converter:
    SIGMA_COEFF = 3
    RELU_SCALE_TYPES = [lambda x: torch.zeros(x.shape, dtype=torch.bool), lambda x: x < 0, lambda x: x < 1]
    SCALE_TYPE = RELU_SCALE_TYPES[2]
    DATA_LOADER = None
    TEST_LOADER = None
    ntest = None
    ndata = None

    def __init__(self, data, ndata, test, ntest, sigma=3, scale_type=2):
        Converter.DATA_LOADER = data
        Converter.ndata = ndata
        Converter.TEST_LOADER = test
        Converter.ntest = ntest
        Converter.SIGMA_COEFF = sigma
        Converter.SCALE_TYPE = Converter.RELU_SCALE_TYPES[scale_type]

    @staticmethod
    def data_driven_initialization(model, n_samples=5, device=None):
        """
        Initialization of SBN from real-valued model: resulting SBN has an affine layer after every linear layer,
        which initialized with scale and bias with effect of batch normalization
        :param model: nn.Sequential, real-valued ReLU network
        :param n_samples: number of samples for every data instance
        :return: nn.Sequential, initialized SBN
        """
        new_model = []
        last_index = Converter.last_block_index(model)
        fw_method = ForwardSample(n_samples=n_samples, device=device)
        x0, _ = next(iter(Converter.DATA_LOADER))
        x1 = fw_method.init_repr(x0)

        for i, layer in enumerate(model):
            if 0 < i < last_index and isinstance(layer, torch.nn.Conv2d):
                layer_sbn = TernaryConv(layer.in_channels, layer.out_channels,
                                        kernel_size=layer.kernel_size, stride=layer.stride,
                                        padding=layer.padding)
                weights = torch.rand_like(layer.weight.data) * 2 - 1
                layer_sbn.set_probs(weights, scale=False)

            elif (isinstance(layer, torch.nn.LeakyReLU) or isinstance(layer, torch.nn.ReLU)) and i < last_index:
                layer_sbn = NoisyStepFunction()

            elif isinstance(layer, torch.nn.modules.batchnorm.BatchNorm2d):
                x1_mean = x1.mean([0, 2, 3]).detach()
                x1_var = x1.var([0, 2, 3]).detach()
                # Want that X1*scale + bias ~ N(0,1)
                scale = 1 / torch.sqrt(x1_var + layer.eps)
                bias = -x1_mean * scale
                layer_sbn = AffineScaleBias(scale=scale.view(1, -1, 1, 1), bias=bias.view(1, -1, 1, 1))
            else:
                warnings.warn(f'{layer.__class__.__name__} at index {i} might need special implementation for SBN')
                layer_sbn = copy.deepcopy(layer)

            x1 = layer_sbn.forward(x1)
            new_model.append(layer_sbn)

        return nn.Sequential(*new_model)

    @staticmethod
    def random_initialization(model, want_BN=True):
        """
        Produces random initialization of SBN
        :param model: nn.Sequential, real-valued ReLU network
        :return: nn.Sequential, initialized SBN
        """
        new_model = []
        last_index = Converter.last_block_index(model)

        for i, layer in enumerate(model):
            if 0 < i < last_index and isinstance(layer, torch.nn.Conv2d):
                sbn_conv = TernaryConv(layer.in_channels, layer.out_channels,
                                        kernel_size=layer.kernel_size, stride=layer.stride,
                                        padding=layer.padding)
                weights = torch.rand_like(layer.weight.data) * 2 - 1
                sbn_conv.set_probs(weights, scale=False)
                new_model += [sbn_conv]
                if want_BN:
                    new_model += [torch.nn.BatchNorm2d(layer.out_channels)]
                else:
                    new_model += [AffineScaleBias(scale=sbn_conv.weight.new_ones(layer.out_channels), bias=sbn_conv.weight.new_zeros(layer.out_channels))]

            elif (isinstance(layer, torch.nn.LeakyReLU) or isinstance(layer, torch.nn.ReLU)) and i < last_index:
                new_model += [NoisyStepFunction()]

            elif isinstance(layer, torch.nn.modules.batchnorm.BatchNorm2d):
                pass

            else:
                new_model.append(layer)

        return nn.Sequential(*new_model)



    @staticmethod
    def eliminate_batchnorm(model):
        new_model = []
        for i, layer in enumerate(model):

            if isinstance(layer, torch.nn.modules.batchnorm.BatchNorm2d):
                conv_layer = new_model[-1]
                weight = conv_layer.weight
                bias = conv_layer.bias
                conv_layer.weight.data = weight * layer.weight.reshape(weight.shape[0], 1, 1, 1) \
                                         / np.sqrt(layer.running_var + layer.eps).reshape(weight.shape[0], 1, 1, 1)
                conv_layer.bias.data = layer.bias + (bias - layer.running_mean) * layer.weight / np.sqrt(
                    layer.running_var + layer.eps)
                new_model[-1] = conv_layer
                continue

            new_model.append(copy.deepcopy(layer))

        return nn.Sequential(*new_model)


    @staticmethod
    def swap_maxpooling(model):
        new_model = [None for i in range(len(model))]
        for i, layer in enumerate(model):

            if isinstance(layer, nn.MaxPool2d) and 'activation' in str(new_model[i - 1].__class__):
                new_model[i] = new_model[i - 1]
                new_model[i - 1] = layer
            else:
                new_model[i] = layer
        return nn.Sequential(*new_model)

    @staticmethod
    def last_block_index(net):
        """
        Returns the index of last block
        :param net: nn.Sequential
        :return: index of the last block, so we don't binarize it
        """
        last_i = -1
        for i, layer in enumerate(net):
            if isinstance(layer, torch.nn.LeakyReLU) or isinstance(layer, torch.nn.ReLU):
                last_i = i
        return last_i

    @staticmethod
    def find_max_weights(weights):
        max_values = torch.ones(weights.shape[0])
        for out_chan in range(weights.shape[0]):
            max_values[out_chan] = torch.abs(weights[out_chan, :, :, :]).max()
        return max_values.reshape(weights.shape[0], 1, 1, 1)


    @staticmethod
    def find_relu_output_max(relu_input):
        x = relu_input.clone()
        m1 = torch.mean(x, (0, 2, 3))
        m2 = torch.var(x, (0, 2, 3))
        scale = (m1 + Converter.SIGMA_COEFF * torch.sqrt(m2)).view(1, -1, 1, 1)
        # do not scale up prea-activations and do not rescale negative preactivations
        scale[Converter.SCALE_TYPE(scale)] = 1.0
        return scale

    @staticmethod
    def calculate_bias_scale_conv(x0, x1):
        mu0, var0 = torch.mean(x0, (0, 2, 3)), torch.var(x0, (0, 2, 3))
        mu1, var1 = torch.mean(x1, (0, 2, 3)), torch.var(x1, (0, 2, 3))
        scale = torch.sqrt(var0 / var1)
        bias = mu0 - scale * mu1
        return scale.view(1, -1, 1, 1), bias.view(1, -1, 1, 1)

    @staticmethod
    def plot_weights(w1, w2):
        fig = go.Figure()
        fig.add_trace(go.Histogram(x=w1.flatten().detach().numpy(),
                                   marker_color='red', name='real-valued'))
        fig.add_trace(go.Histogram(x=w2.flatten().detach().numpy(),
                                   marker_color='blue', name='binary'))
        fig.show()

    @staticmethod
    def scale_single_layer(relu_net, index_act, sbn_net=None, number_copy=4, plot=False, plot_weights_new=False,
                           flag=False, device=None):
        """
        Changes single real-valued block to a binary one
        :param relu_net: nn.Sequential, referenced ReLu network
        :param index_act: int, index of the start of real-valued block (ReLu -> Conv)
        :param sbn_net: nn.Sequential, partial SBN or None
        :param number_copy: int, number of data copies for averaging over SBN noise
        :param plot: boolean, plots statistics of outputs over all channels if True
        :param flag: boolean, prints statistics if True
        :return: nn.Sequential, identical to `sbn_net`, but binary block at `index_act`
        """
        if sbn_net is None:
            sbn_net = relu_net
        x0, _ = next(iter(Converter.DATA_LOADER))
        fw_method = ForwardSample(number_copy, device)
        x1 = fw_method.init_repr(x0)
        new_layers = []
        scale_linear = None
        if plot:
            fig = make_subplots(rows=2, cols=(len(relu_net[index_act - 1:]) - 2) // 2, start_cell="top-left")
            legend = True

        for i, layer in enumerate(sbn_net):
            x0 = relu_net[i](x0)

            if i != index_act and i != index_act + 1:
                x1 = fw_method.forward(layer, x_repr=x1)
                new_layers.append(copy.deepcopy(layer))

            elif i == index_act:
                activation = NoisyStepFunction()
                # averaging over sbn
                avg_sbn = fw_method.mean(x1)
                scale_linear = Converter.find_relu_output_max(avg_sbn)

                # adding affine layer to scale input before activation
                affine = AffineScaleBias(scale=1 / scale_linear, bias=torch.zeros(scale_linear.shape))
                act_block = ActBlock(*[affine, activation])
                x1 = fw_method.forward(act_block, x_repr=x1)
                new_layers.append(act_block)

            else:
                # here dim of scale_linear is 1 x n_in x 1 x 1
                weights = layer.weight.data * scale_linear
                # here dim of scale_weights is n_out x 1 x 1 x 1
                scale_weights = Converter.find_max_weights(weights)

                weights_ternary = weights / scale_weights
                if plot_weights_new:
                    Converter.plot_weights(layer.weight.data, weights_ternary)

                ternary_conv = TernaryConv(layer.in_channels, layer.out_channels,
                                           kernel_size=layer.kernel_size, stride=layer.stride,
                                           padding=layer.padding)
                ternary_conv.set_probs(weights_ternary, scale=False)
                x1 = fw_method.forward(ternary_conv, x_repr=x1)

                # averaging over sbn
                avg_sbn = fw_method.mean(x1)
                scale, shift = Converter.calculate_bias_scale_conv(x0, avg_sbn)
                affine = AffineScaleBias(scale=scale, bias=shift)
                x1 = fw_method.forward(affine, x_repr=x1)
                new_layers.append(ConvBlock(*[ternary_conv, affine]))

            if plot and i <= 17 and i >= index_act - 1:
                j = i
                i = i - index_act + 1

                avg_sbn = fw_method.mean(x1)
                fig.append_trace(go.Histogram(x=x0[:, 3, :, :].flatten().detach().numpy(),
                                              marker_color='red', showlegend=legend, name='real-valued',
                                              legendgroup='real-valued', xbins=dict(size=0.3), opacity=0.5), i % 2 + 1, i // 2 + 1)
                fig.append_trace(go.Histogram(x=avg_sbn[:, 3, :, :].flatten().detach().numpy(),
                                              marker_color='blue', name='binary', legendgroup='binary',
                                              showlegend=legend, xbins=dict(size=0.3), opacity=0.5), i % 2 + 1, i // 2 + 1)
                legend = False
                if i == 1:
                    fig.update_xaxes(title_text=f"{j}: Binary Activation", row=i % 2 + 1, col=i // 2 + 1, )
                elif i == 2:
                    fig.update_xaxes(title_text=f"{j}: Ternary conv", row=i % 2 + 1, col=i // 2 + 1)
                else:
                    fig.update_xaxes(title_text=f"{j}: "+layer.__class__.__name__, row=i % 2 + 1, col=i // 2 + 1)
            if flag:
                print(i, layer.__class__.__name__)
                print(x1.mean(), x0.mean())
                print(x1.std(), x0.std())

        if plot:
            fig.update_yaxes(showticklabels=False)
            fig.update_layout(font_size=14)
            fig.show()
        return nn.Sequential(*new_layers)

    @staticmethod
    def test_sbn(sbn: nn.Sequential, number_copy: int = 10, loss=None, loader=None):
        if loader is None:
            loader = Converter.TEST_LOADER
        device = next(sbn.parameters()).device
        sbn.eval()
        fw_method = ForwardSample(number_copy, device)
        correct = 0
        n_data = 0
        L = []
        with torch.no_grad():
            for i, (data, target) in enumerate(loader):
                data, target = data.to(device), target.to(device)
                y = fw_method.forward(sbn, x=data)
                ym = fw_method.log_mean(y)
                if loss is not None:
                    L.append(loss(ym, target).item())
                pred = ym.data.max(1, keepdim=True)[1]
                correct += pred.eq(target.data.view_as(pred)).sum()
                print(pred)
                n_data += data.shape[0]

        correct = correct.item() / n_data

        return correct, np.mean(L)

    @staticmethod
    def train_weights_single_layer(relu_model: nn.Sequential, sbn: nn.Sequential, last_index: int, fw_method,
                                   epochs: int = 10, summary_writer=None, lr=0.001, matching=MatchingLoss,
                                   loss_index=0):
        exp_res = {'epoch': [], 'train_loss': [], 'test_accuracy': []}
        device = fw_method.device
        optimizer = torch.optim.Adam(sbn[last_index - 1:last_index + 1].parameters(), lr=lr)
        print("training\n", sbn[last_index - 1:last_index + 1])

        relu_model.eval()
        batch_index = 0
        for epoch in range(epochs):
            sbn.train()
            exp_res['epoch'].append(epoch)
            print(f"EPOCH: {epoch}")
            epoch_losses = []
            for index, (data, target) in enumerate(Converter.DATA_LOADER):
                data = data.to(device)
                x0 = relu_model[:last_index + loss_index + 1](data.clone())
                x1 = fw_method.forward(sbn[:last_index + loss_index + 1], x=data)
                loss = fw_method.forward_loss(matching, x1, x0).mean()

                epoch_losses.append(loss.item())
                if summary_writer is not None:
                    summary_writer.add_scalar("Epoch batch/train", loss.item(), batch_index)
                batch_index += 1

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                break

            print(f'epoch loss: {np.mean(epoch_losses)}')

            if epoch % 10 == 0:
                correct = Converter.test_sbn(sbn, 20)
                print(f'Test acc: {correct}', end='\t')
                if summary_writer is not None:
                    summary_writer.add_scalar("Epoch accuracy/test", correct, epoch)
                exp_res['test_accuracy'].append(correct)

            if summary_writer is not None:
                summary_writer.add_scalar("Epoch loss/train", np.mean(epoch_losses), epoch)
            exp_res['train_loss'].append(np.mean(epoch_losses))
        return sbn, exp_res

    @staticmethod
    def train_weights_single_layer_window(relu_model: nn.Sequential, sbn: nn.Sequential, last_index: int, fw_method,
                                          window: int = 5, summary_writer=None, lr=0.001, matching=torch.nn.MSELoss(),
                                          loss_index=0, thresh=0.01 / 5):
        exp_res = {'epoch': [], 'train_loss': [], 'test_accuracy': [], 'train_accuracy': []}
        device = fw_method.device
        optimizer = torch.optim.Adam(sbn[:last_index + 1].parameters(), lr=lr)

        relu_model.eval()
        batch_index = 0
        epoch = 0
        while True:
            sbn.train()
            exp_res['epoch'].append(epoch)
            print(f"EPOCH: {epoch}")
            epoch_losses = []
            correct_train = 0
            for index, (data, target) in enumerate(Converter.DATA_LOADER):
                data, target = data.to(device), target.to(device)
                x0 = relu_model[:last_index + loss_index + 1](data.clone())
                x1 = fw_method.forward(sbn[:last_index + loss_index + 1], x=data)
                loss = fw_method.forward_loss(matching, x1, x0).mean()

                y = fw_method.forward(sbn[last_index + loss_index + 1:], x_repr=x1)
                ym = fw_method.mean(y.exp())
                pred = ym.data.max(1, keepdim=True)[1]
                correct_train += pred.eq(target.data.view_as(pred)).sum()

                epoch_losses.append(loss.item())
                if summary_writer is not None:
                    summary_writer.add_scalar("Epoch batch/train", loss.item(), batch_index)
                batch_index += 1

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            exp_res['train_accuracy'].append(correct_train.item() / Converter.ndata)
            print(f'epoch loss: {np.mean(epoch_losses)}')

            if epoch % 5 == 0:
                correct, _ = Converter.test_sbn(sbn, 20)
                print(f'Test acc: {correct}', end='\t')
                if summary_writer is not None:
                    summary_writer.add_scalar("Epoch accuracy/test", correct, epoch)
                exp_res['test_accuracy'].append(correct)

            if summary_writer is not None:
                summary_writer.add_scalar("Epoch loss/train", np.mean(epoch_losses), epoch)
            exp_res['train_loss'].append(np.mean(epoch_losses))

            epoch += 1

            if epoch > window:
                cur_change = np.mean(exp_res['train_accuracy'][-window::]) - np.mean(
                    exp_res['train_accuracy'][-window - 1:-1])
                if cur_change < thresh:
                    print(f"breaking with cur_change = {cur_change} on epoch {epoch}")
                    break
                else:
                    print(f"cur_change = {cur_change}")

        return sbn, exp_res

    @staticmethod
    def initialize_network(relu_net: nn.Sequential, device: torch.device = torch.device('cpu'),
                           epochs_matching: int = 5,
                           number_copy: int = 4, thresh=0.01 / 5, window=False, relu_quantile=None):
        relu_net = relu_net.to(device)
        sbn = copy.deepcopy(relu_net).to(device)
        last_block = Converter.last_block_index(relu_net)
        results = []
        for i, layer in enumerate(relu_net):
            print(i, last_block)

            if i >= last_block:
                break

            # skipping if it is a linear layer
            if i % 2 == 0 or not (isinstance(layer, torch.nn.LeakyReLU) or isinstance(layer, torch.nn.ReLU)):
                print(layer.__class__.__name__)
                continue

            # converting one block to sbn
            sbn = Converter.scale_single_layer(relu_net, i, sbn_net=sbn, device=device, relu_quantile=relu_quantile)
            print(f"{i} - converted")
            fw_method = ForwardSample(n_samples=number_copy, device=device)
            if not window:
                sbn, res = Converter.train_weights_single_layer(relu_net, sbn, last_index=i + 1, loss_index=1,
                                                                matching=torch.nn.MSELoss(),
                                                                fw_method=fw_method, epochs=i)
            else:
                sbn, res = Converter.train_weights_single_layer_window(relu_net, sbn, last_index=i + 1, loss_index=1,
                                                                       matching=torch.nn.MSELoss(),
                                                                       fw_method=fw_method, window=epochs_matching,
                                                                       thresh=thresh)

            results.append(res)
            print(res)
            print(f"index = {i}, accuracy matching = {accuracy_block}, total epochs = {res['epoch'][-1]}")

        return sbn, results

    @staticmethod
    def train_sbn(sbn, lr=0.005, number_copy=5, epochs=20, summary_writer=None, device=None, last_epoch=0, last_batch=0,
                  val_loader=None):
        print("training whole net!")
        exp_res = {'epoch': [], 'train_loss': [], 'test_accuracy': [], 'train_accuracy': [], "test_loss": []}

        optimizer = torch.optim.Adam(sbn.parameters(), lr=lr)
        fw_method = ForwardSample(number_copy, device=device)
        loss = nn.CrossEntropyLoss()
        batch_index = 0

        for epoch in range(epochs):
            sbn.train()
            print(f"\nEPOCH: {epoch + last_epoch}")
            total_correct = 0
            L = []
            for i, (data, target) in enumerate(Converter.DATA_LOADER):
                if device is not None:
                    data, target = data.to(device), target.to(device)

                y = fw_method.forward(sbn, x=data)
                y = fw_method.log_mean(y)
                pred = y.data.max(1, keepdim=True)[1]

                batch_correct = pred.eq(target.data.view_as(pred)).sum().item()
                total_correct += batch_correct

                l = loss(y, target).mean()
                L.append(l.item())

                optimizer.zero_grad()
                l.backward()
                optimizer.step()

                if summary_writer is not None:
                    summary_writer.add_scalar("Batch stat/loss", l.item(), batch_index + last_batch)
                    summary_writer.add_scalar("Batch stat/accuracy", batch_correct / 100, batch_index + last_batch)
                batch_index += 1

            print(f'epoch train accuracy: {total_correct / Converter.ndata}')
            exp_res["epoch"].append(epoch + last_epoch)
            exp_res["train_accuracy"].append(total_correct / Converter.ndata)
            exp_res["train_loss"].append(np.mean(L))

            if summary_writer is not None:
                summary_writer.add_scalar("Epoch loss/train", np.mean(L), epoch + last_epoch)
                summary_writer.add_scalar("Epoch accuracy/train", total_correct / Converter.ndata, epoch + last_epoch)

            sbn.eval()

            if epoch % 5 == 0:
                acc_test, test_loss = Converter.test_sbn(sbn, number_copy=20, loss=loss)
                exp_res['test_accuracy'].append(acc_test)
                exp_res['test_loss'].append(test_loss)

                if val_loader is not None:
                    acc_val, loss_val = Converter.test_sbn(sbn, number_copy=20, loss=loss, loader=val_loader)

                if summary_writer is not None:
                    if val_loader is not None:
                        summary_writer.add_scalar("Epoch accuracy/validation", acc_val, epoch + last_epoch)
                        summary_writer.add_scalar("Epoch loss/validation", loss_val, epoch + last_epoch)

                    summary_writer.add_scalar("Epoch accuracy/test", acc_test, epoch + last_epoch)
                    summary_writer.add_scalar("Epoch loss/test", test_loss, epoch + last_epoch)

            print(f'epoch test accuracy: {acc_test}')

        return sbn, exp_res


class Identity(torch.nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        return x
