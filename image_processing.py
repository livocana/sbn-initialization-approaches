import numpy as np
import torchvision
import torch
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler

# transform + augmentation are used only for the training set
TRANSFORM_AUGMENT = transforms.Compose([transforms.RandomHorizontalFlip(),
                                        transforms.RandomAffine(0, translate=(0.15, 0.15)),
                                        transforms.ToTensor(),
                                        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261)),
                                         ])

# simple transform is used for the test and validation sets
TRANSFORM = transforms.Compose([transforms.ToTensor(),
                                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))])


class DatasetLoader:
    CIFAR10_CLASSES = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

    def __init__(self, train_batch_size=32, test_batch_size=4):
        self.ntrain = 0
        self.nval = 0
        self.ntest = 0

        self.train_loader = None
        self.val_loader = None
        self.test_loader = None

        self.train_batch_size  = train_batch_size
        self.test_batch_size = test_batch_size

    def load_cifar10(self, val_size=0.1):
        trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=False, transform=TRANSFORM_AUGMENT, )
        validationset = torchvision.datasets.CIFAR10(root='./data', train=True, download=False, transform=TRANSFORM)

        self.train_validation_split(trainset, validationset, val_size)

        testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=False, transform=TRANSFORM)
        self.test_loader = torch.utils.data.DataLoader(testset, batch_size=self.test_batch_size, shuffle=False, num_workers=2)
        self.ntest = len(testset)
        return self

    def train_validation_split(self, trainset, validationset, val_size=0.1):
        n_train_data = len(trainset)
        indices = list(range(n_train_data))
        split = int(np.floor(val_size * n_train_data))
        np.random.shuffle(indices)

        train_indices, val_indices = indices[split:], indices[:split]
        self.ntrain = len(train_indices)
        self.nval = len(val_indices)

        train_sampler = SubsetRandomSampler(train_indices)
        valid_sampler = SubsetRandomSampler(val_indices)

        self.train_loader = torch.utils.data.DataLoader(dataset=trainset, batch_size=self.train_batch_size, sampler=train_sampler)
        self.validation_loader = torch.utils.data.DataLoader(dataset=validationset, batch_size=self.train_batch_size, sampler=valid_sampler)

        return self.train_loader, self.validation_loader


    def get_ntrain(self):
        return self.ntrain

    def get_ntest(self):
        return self.ntest

    def get_nval(self):
        return self.nval

    def get_trainloader(self):
        return self.train_loader

    def get_testloader(self):
        return self.test_loader

    def get_valloader(self):
        return self.validation_loader


def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = inp
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(20)

