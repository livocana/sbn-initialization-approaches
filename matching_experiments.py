import numpy as np
import torch
from architecture import *
import pickle
import sys
from train_cnn import *

def save_result(result, filename):
    with open(filename, 'wb') as file:
        pickle.dump(result, file)


layer_index = int(sys.argv[1])
loss_type = int(sys.argv[2])
loss_layer = int(sys.argv[3])
model_name = sys.argv[4]
filename = f'res-layer{layer_index}-losstype{loss_type}-losslayer{loss_layer + layer_index}'

epochs = 3
lr = 0.001

dataloader = DatasetLoader(train_batch_size=256, test_batch_size=100).load_cifar10()
test_loader = dataloader.get_testloader()
ntest = dataloader.get_ntest()

model = ConvolutionalNet().get_layers()
model.load_state_dict(torch.load("models/allcnn_model_120.pt", map_location=torch.device('cpu')))

converter = Converter(dataloader.get_trainloader(), dataloader.get_ntrain(), dataloader.get_testloader(), dataloader.get_ntest(), sigma=1.5, scale_type=2)
model = converter.eliminate_batchnorm(model)

device = torch.device(0)
model_new = torch.load(f"converted_models_block/{model_name}.pt", map_location=torch.device('cpu'))
writer = SummaryWriter()

fw_method = ForwardSample (n_samples=10, device=device)
losses = [MatchingLoss(fw_method), torch.nn.MSELoss()]
model_new_trained, stats = converter.train_weights_single_layer(model, model_new, layer_index, fw_method, epochs=epochs,
                                                                summary_writer=writer, lr=lr, loss_index=loss_layer,     matching=losses[loss_type])
print(stats)
writer.flush()
writer.close()

save_result([{'layer_index': layer_index, 'loss_layer': loss_layer, 'loss_type': loss_type, 'lr': lr, 'model_name': model_name}, stats], filename)
