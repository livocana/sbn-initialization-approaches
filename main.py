import numpy as np
import torch
from architecture import *
import pickle
from torch.utils.tensorboard import SummaryWriter


from train_cnn import *


dataloader = DatasetLoader(train_batch_size=256, test_batch_size=100).load_cifar10()
converter = Converter(dataloader.get_trainloader(), dataloader.get_ntrain(),
                      dataloader.get_valloader(), dataloader.get_nval())


"""loading VGG16 for CIFAR"""
# model = torch.load("models/vgg16_cifar.pt", map_location=torch.device('cpu'))
# model = converter.eliminate_batchnorm(model)
# model = converter.swap_maxpooling(model)
# print(model)

"""loading AllCNN-89 for CIFAR"""
model = ConvolutionalNet().get_layers()
model.load_state_dict(torch.load("models/allcnn_model_120.pt", map_location=torch.device('cpu')))


"""loading AllCNN-94.3 for CIFAR"""
# model = torch.load("models/AllCNN-94.3.pt", map_location=torch.device('cpu'))


"""creating a random initialization with BN from the model"""
random_init_bn = converter.random_initialization(model, want_BN=True)
print(random_init_bn)
torch.save(random_init_bn, "/Applications/SBN/sbn-initialization-approaches/initialized/random_init_BN.pt")


"""creating a data driven random initialization without BN from the model"""
# random_init_nobn = converter.data_driven_initialization(model)
# print(random_init_nobn)
# torch.save(random_init_nobn, "/Applications/SBN/sbn-initialization-approaches/initialized/random_data_driven_init.pt")

"""creating a smart initialization"""
# device = torch.device('cpu')
# model_new, results = Converter.initialize_network(model, device, epochs_matching=5, number_copy=4,
#                                                   thresh=0.01/5, window=True, relu_quantile=0.8)


"""training for the final loss"""
# device = torch.device('cpu')
# model_trained, res = converter.train_sbn(model_new, number_copy=1, epochs=400, device=device, lr=0.001,
#                                          val_loader=dataloader.get_valloader())

"""testing SBN model for final loss"""
# test_acc, _ = converter.test_sbn(random_init_nobn, number_copy=2)

