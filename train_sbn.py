import numpy as np
import torch
import pickle
from torch.utils.tensorboard import SummaryWriter

from architecture import *
from train_cnn import *


dataloader = DatasetLoader(train_batch_size=256, test_batch_size=100).load_cifar10()
test_loader = dataloader.get_testloader()
ntest = dataloader.get_ntest()

converter = Converter(dataloader.get_trainloader(), dataloader.get_ntrain(), dataloader.get_testloader(),
                      dataloader.get_ntest(), sigma=1.5, scale_type=2)


device = torch.device("cuda:0")

model_new = torch.load("initialized/random_initialization.pt")
model_new = model_new.to(device)
writer = SummaryWriter(log_dir='./runs-net/random-random')
model_new_trained, res = converter.train_sbn(model_new, number_copy=5, epochs=150, summary_writer=writer, device=device, lr=0.001)
torch.save(model_new_trained, f"initialized/random-sbn-5copies-trained.pt")
with open("initialized/random-sbn-5copies-trained-stat.pickle", "wb") as file:
    pickle.dump(res, file)

writer.flush()
writer.close()