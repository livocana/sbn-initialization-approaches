import torch
import torch.nn as nn
import numpy as np
import copy


class BinConv2D(nn.modules.conv.Conv2d):
    
    def __init__(self, in_channels, out_channels, *args, weight_type='prob', **kwargs):
        super().__init__(in_channels, out_channels, *args, bias=False, **kwargs)
        self.weight_type = weight_type
    
    def forward(self, x):
        return super().forward(x)


class AffineScaleBias(nn.Module):
    
    def __init__(self, scale, bias):
        super().__init__()
        self.scale = nn.Parameter(scale)
        self.bias = nn.Parameter(bias)
    
    def forward(self, x):
        return self.scale * x + self.bias


class SignFunction(nn.Module):
    
    def __init__(self, noise_type='uniform'):
        super().__init__()
        self.noise_type = noise_type
    
    def forward(self, x):
        z = torch.rand(x.shape)
        return torch.sign(x - z)


class NoisyStepFunction(nn.Module):

    def __init__(self, noise_type='uniform'):
        super().__init__()
        self.noise_type = noise_type

    def forward(self, x):
        z = torch.rand(x.shape).to(x.device)
        dy = torch.clamp(x, 0, 1).to(x.device)
        y = (x > z) * torch.ones_like(x) + (dy - dy.detach())
        return y



class TernaryConv(nn.Module):
    
    def __init__(self, in_channels, out_channels, *args, weight_type='prob', **kwargs):
        super().__init__()
        self.layer = BinConv2D(in_channels, out_channels, *args, weight_type=weight_type, **kwargs)
        self.shape = self.layer.weight.shape
        self.layer.weight = None
        self.layer.bias = None
        self.eta_plus = nn.Parameter(torch.zeros(self.shape))
        self.eta_minus = nn.Parameter(torch.zeros(self.shape))

    @property
    def p_plus(self):
        return self.eta_plus.sigmoid()

    @property
    def p_minus(self):
        return self.eta_minus.sigmoid()

    @p_plus.setter
    def p_plus(self, val):
        self.eta_plus.data = torch.log(val) - torch.log1p(-val)

    @p_minus.setter
    def p_minus(self, val):
        self.eta_minus.data = torch.log(val) - torch.log1p(-val)

    @staticmethod
    def find_scale(w, q=1):
        return torch.quantile(w.view(w.shape[1], -1), q=q, dim=1).view(1, -1, 1, 1)

    def set_probs(self, weights, scale=True, q=1):
        max_w = None
        if scale:
            max_w = self.find_scale(weights, q=q)
            weights = weights / max_w

        self.p_plus = ((weights > 0) * weights).clamp(min=0.01, max=0.99)
        self.p_minus = ((weights < 0) * (-weights)).clamp(min=0.01, max=0.99)

        return max_w

    def generate_weight_ST(self, eta):
        w = eta.sigmoid().bernoulli() + eta - eta.detach()
        return w
    
    def forward(self, x):
        assert hasattr(self, 'p_plus'), "Please call set_probs first"
        w_plus = self.generate_weight_ST(self.eta_plus)
        w_minus = self.generate_weight_ST(self.eta_minus)
        weight = w_plus - w_minus
        y = torch.conv2d(x,weight,bias=None,stride=self.layer.stride,padding=self.layer.padding,
                         dilation=self.layer.dilation,groups=self.layer.groups)
        return y.to(x.device)
    
    def multi_sample_fw(self, x, n_samples):
        """
        :param x: expanded data batch, containing concatenated n_samples of data batches
        :param n_samples: number of samples
        :return: Ternary convolution W*x with W sampled as many times
        implementation method: using grouped convolutions
        """
        assert hasattr(self, 'p_plus'), "Please call set_probs first"
        #todo: check group convolution calculations
        # sz = list(self.p_plus.shape)
        # sz[0] *= n_samples
        # print(self.p_plus.shape)
        # W = self.p_plus.repeat(n_samples, 1, 1, 1, 1).bernoulli() - self.p_minus.repeat(n_samples, 1, 1, 1, 1).bernoulli()
        # layer_current.groups = n_samples
        # layer_current.in_channels *= n_samples
        # layer_current.out_channels *= n_samples
        # layer_current = copy.deepcopy(self.layer)
        # layer_current.weight.data = W
        conv_realisations = []
        sz = int(x.shape[0] / n_samples)
        for conv_i in range(1, n_samples+1):
            start, end = (conv_i - 1) * sz, conv_i * sz
            conv_realisations.append(self.forward(x[start:end, :, :, :]))
        y = torch.cat(conv_realisations, dim=0).to(x.device)
        return y

class ActBlock(nn.Sequential):
    pass


class ConvBlock(nn.Sequential):
    pass


class MatchingLoss(nn.Module):

    def __init__(self, forward_method):
        super().__init__()
        self.fw_method = forward_method

    def forward(self, x1: torch.Tensor, x0: torch.Tensor): # x1: [S*B x W x H x C]
        """
        :param x1: input
        :param x0: target
        :return: unbiased loss as in 'Converting real-valued networks to SBN'
        """
        avg_sbn = self.fw_method.mean(x1) # [B x W x H x C]
        x1_reshaped = self.fw_method.reshape2copies(x1) # [S x B x W x H x C]
        s = self.fw_method.n_samples
        avg_sbn_square = (x1_reshaped.sum(dim=0)**2 - (x1_reshaped ** 2).sum(dim=0)) / (s * (s - 1))
        return avg_sbn_square - 2 * x0 * avg_sbn + x0 ** 2 # [B x W x H x C]


class AttentionLoss(nn.Module):

    def __init__(self):
        super().__init__()

    def forward(self, avg_sbn, x0):
        """
        :param x1: input features, average over sbn
        :param x0: target features
        :return: attention loss as in https://arxiv.org/pdf/2003.11535.pdf
        """
        qs = (avg_sbn ** 2).sum(0)
        qt = (x0 ** 2).sum(0)
        qs_norm = torch.sqrt((qs ** 2).sum())
        qt_norm = torch.sqrt((qt ** 2).sum())
        return torch.abs(qs / qs_norm - qt / qt_norm)




class ForwardSample:
    InternalRepr = torch.Tensor

    def __init__(self, n_samples, device):
        self.n_samples = n_samples
        self.device = device

    def init_repr(self, x: torch.Tensor):
        # x1 = torch.repeat_interleave(x.clone(), self.n_samples, dim=0)
        x1 = x.repeat(self.n_samples, 1, 1, 1).to(self.device)
        return x1

    def reshape2copies(self, x_repr: InternalRepr):
        if x_repr.shape[0] != self.n_samples:
            dims = list(x_repr.shape)
            shape = [self.n_samples, int(x_repr.shape[0] / self.n_samples)] + dims[1:]
            x_repr = x_repr.reshape(shape)
        return x_repr

    def forward(self, layers: [nn.Sequential, nn.Module], x: torch.Tensor=None, x_repr: InternalRepr = None) -> InternalRepr:
        """ input:
            layers -- network or a piece of network made of SBN layers or a single layer,
            x_repr -- internal representation of the propagation method (tensor of multiple samples, or for other method mean and variance per component).
            Either x or x_repr can be supplied

            output: x_repr -- internal representation of the result of propagation
        """
        if x_repr is None:
            x1 = self.init_repr(x)
        else:
            x1 = x_repr
        if isinstance(layers, nn.Sequential):
            # propagate the layers
            for layer in layers:
                x1 = self.forward_layer(layer, x1)  # deterministic layers (e.g affine) and coordinate-wise layers should work fine for expanded batch
        else:
            x1 = self.forward_layer(layers, x1)  # deterministic layers (e.g affine) and coordinate-wise layers should work fine for expanded batch
        return x1

    def forward_layer(self, layer, x):
        if isinstance(layer, nn.Sequential):
            x = self.forward(layer, x_repr=x)
        elif isinstance(layer, TernaryConv):
            x = layer.multi_sample_fw(x, self.n_samples)
        else:
            x = layer.forward(x)
        return x

    def mean(self, x_repr: InternalRepr) -> torch.Tensor:
        """ computes SBN mean using the earlier computed internal representation x_repr"""
        x_repr = self.reshape2copies(x_repr)
        return x_repr.mean(dim=0) #dim=0

    def forward_loss(self, loss, input, target):
        if isinstance(loss, MatchingLoss):
            return loss.forward(input, target)
        else:
            return loss(self.mean(input), target)

    def log_mean(self, x_repr):
        x_repr = self.reshape2copies(x_repr)
        return torch.logsumexp(x_repr, dim=0) - np.log(self.n_samples)

    def var(self, x_repr: InternalRepr) -> torch.Tensor:
        pass

    def std(self, x_repr: InternalRepr) -> torch.Tensor:
        pass


""" example usage:
    e_method = fw_sample(n_samples=40)
    x1 = e_method.forward(SBNNet[0:index_act], x0)
    avg_sbn = e_method.mean(x1)
    block = SignBlock(sign_activation, affine)
    x1 = e_method.forward(block x1)
    avg_sbn = e_method.mean(x1)
"""
